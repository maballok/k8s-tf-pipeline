#!/bin/bash
# Postinstall for Worker Node 

# Create User
useradd -s /bin/bash -c "Student" -m student
echo "Passw0rd" | passwd --stdin student
# Set sudo
echo "student ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

## K8S
yum -y install git jq
# Clone Repo
git clone https://gitlab.com/pety-linux/k8s/k8s-install.git
# Run K8S Installation Script
bash k8s-install/install-node-containerd.sh
#
# Join to the Cluster
sleep 4m
wget -O - http://master.pety.net/initcommand | bash
##
