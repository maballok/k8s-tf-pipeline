resource "aws_route53_zone" "route53_zone_private1" {
  name = "pety.net"

  vpc {
    vpc_id = aws_vpc.vpc.id
  }

  force_destroy = true

  tags = {
    Name = "pety.net"
  }
}
